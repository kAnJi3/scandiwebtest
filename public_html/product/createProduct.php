<?php require_once('../../functions.php');
$page_title = "Product add";
include(PROJECT_PATH . '/header.php'); ?>
    <div class="wrapper">
      <div class="header">
        <div class="headerLeft">
          <h1> Product Add</h1>
        </div>
        <div class="headerRight">
          <button type="submit" class="button formSubmit">Save</button>
          <a href="index" class="button">Cancel</a>
        </div>
      </div>
      <div id="productForm" class="formWrapper">
        <form method="post">
          <input type="text" class="form-control" hidden name="function" id="function" value="add">
          <div class="row">
            <label for="SKU">SKU</label>
            <div class="inputWrapper">
              <input type="text" class="form-control" name="SKU" id="SKU" placeholder="JVC200123">
            </div>
          </div>
          <div class="row">
            <label for="Name">Name</label>
            <div class="inputWrapper">
              <input type="text" class="form-control" name="Name" id="Name" placeholder="">
            </div>
          </div>
          <div class="row">
            <label for="Price">Price($)</label>
            <div class="inputWrapper">
              <input type="number" min="0" step="0.01" class="form-control" name="Price" id="Price" placeholder="">
            </div>
          </div>
          <div class="row">
            <label for="Type"> Product type selector</label>
            <div class="inputWrapper">
              <select class="form-control" name="Type" id="Type">
                <option value="DVD_disc">DVD-disc</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
              </select>
            </div>
          </div>
          <div class="Attributes">
          </div>
        </form>
      </div>
    </div>
  <?php include(PROJECT_PATH . '/footer.php'); ?>
