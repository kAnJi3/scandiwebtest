<?php require_once('../../functions.php');
$page_title = "Product list";
include(PROJECT_PATH . '/header.php'); ?>
    <div class="wrapper">
      <div class="header">
        <div class="headerLeft">
          <h1>Product List</h1>
        </div>

        <div class="headerRight">
          <a href="createProduct" class="button">Add a product</a>
          <button type="button" class="button deleteProducts">Delete Items</button>
        </div>
      </div>
      <div class="productList">
        <?php $db = new db();
        $db->getProducts(); ?>
      </div>

    </div>
  </div>
<?php include(PROJECT_PATH . '/footer.php'); ?>
