$('.formSubmit').click( function(e) {
  e.preventDefault();
  var data = $("#productForm form").serializeArray();
  $.ajax({
    url: '../../functions.php',
    type: 'post',
    data: data,
    })
    .done(function(response) {
      location.replace("List");
    })
    .fail(function(error) {
      $('.errors').remove();
      console.log(error);
      $('#productForm').prepend(error.responseText);
    })
});

$('#productForm #Type').change( function(e) {
  $.ajax({
    url: '../../functions.php',
    type: 'post',
    data: {Type: $(this).val(), function: 'formAttribute'},
    })
    .done(function(response) {
      $('#productForm .Attributes').html(response);
    })
    .fail(function(error) {
      $('.errors').remove();
      $('#productForm').prepend(error.responseText);
    })

});
$(window).on("load", function() {
  if("#productForm #Type") {
    $("#productForm #Type").trigger("change");
  }
});

$('.deleteProducts').click( function() {
  if($(this).text() == "Delete Items") {
    $(this).text("Mass Delete");
    $.each( $(".productItem"), function () {
      $(this).append('<input type="checkbox" name="product_ids" value="">');
    });
  } else {
    var selectedProducts = [];
    $( ".productItem input:checked" ).each(function() {
      selectedProducts.push($(this).closest(".productItem").data('sku'));
    });
    if(selectedProducts.length) {
      $.ajax({
        url: '../../functions.php',
        type: 'post',
        data: {products:selectedProducts, function:"delete"},
        success: function(response) {
          location.reload();
        }
      });
    }
  }

});
