<?php
require_once('config.php');
require_once('db.php');
function my_autoload($class) {
  if(preg_match('/\A\w+\Z/',$class)) {
    try {
      include 'classes/'. $class . '.php';
    } catch (Exception $e) {
      echo $e->getMessage(), "\n";
    }
  }
}
spl_autoload_register('my_autoload');
function url_for($script_path) {
  if($script_path[0] != '/') {
    $script_path = "/".$script_path;
  }
  return WWW_ROOT . $script_path;
}
if($_POST) {
  if($_POST['function'] == "delete") {
    $db = new db();
    $db->deleteProducts($_POST['products']);
  }
  elseif($_POST['function'] == "add") {
    $product = new $_POST['Type']();
    $errors = $product->validateForm();
    if(!empty($errors)) {
      header('HTTP/1.0 500 Internal Server Error');
      echo '<div class ="errors">';
      echo '<ul>';
      foreach($errors as $error) {
        echo '<li>'.$error.'</li>';
      }
      echo '</ul>';
      echo '</div>';
    } else {
      $product->setProperties($_POST['SKU'], $_POST['Name'], $_POST['Price'], $product->prepareAttributes());
      $db = new db();
      $db->insertProduct($product);
    }
  }
  elseif($_POST['function'] == "formAttribute") {
    $product = new $_POST['Type']();
    $product->printAttributeForm();
  }
}

?>
