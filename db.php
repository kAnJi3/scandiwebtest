<?php
class db  {
  private $connection;
  private function connect() {
    if(!isset($this->connection)) {
      $this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    }
    if($this->connection === false) {
      return mysqli_connect_error();
    }
  }
  private function disconnect() {
    if(isset($this->connection)) {
      mysqli_close($this->connection);
    }
  }
  public function getProducts() {
    $query = "Select * FROM products";
    $this->connect();
    $result = mysqli_query($this->connection,$query);
    while ($row = $result->fetch_assoc()) {
      $Product = new $row['Type']();
      $Product->setProperties($row['SKU'],$row['Name'],$row['Price'],$row['Attributes']);
      $Product->printProduct();
    }
    $result->free_result();
    $this->disconnect();
  }
  public function deleteProducts($products) {
    $this->connect();
    $productsSKU= [];
    foreach($products as $productSKU) {
      $productSKU = trim($productSKU);
      $productSKU = stripslashes($productSKU);
      $productSKU = htmlspecialchars($productSKU);
      $productsSKU[]= mysqli_real_escape_string($this->connection,$productSKU);
    }
    $productsSKU = "'".implode("','",$productsSKU)."'";
    $query = "Delete FROM products WHERE SKU in";
    $query.= "(".$productsSKU.")";
    $result = mysqli_query($this->connection,$query);
    echo $query;
    $this->disconnect();
  }
  public function insertProduct($Product) {
    $this->connect();
    $query = "Insert INTO products(SKU, Name, Price, Type, Attributes) VALUES(";
    $query .= "'" . mysqli_real_escape_string($this->connection,$Product->SKU) . "',";
    $query .= "'" . mysqli_real_escape_string($this->connection,$Product->Name) . "',";
    $query .= "'" . mysqli_real_escape_string($this->connection,$Product->Price) . "',";
    $query .= "'" . mysqli_real_escape_string($this->connection,get_class($Product)) . "',";
    $query .= "'" . mysqli_real_escape_string($this->connection,$Product->prepareAttributes()) . "'";
    $query .=")";
    $result = mysqli_query($this->connection,$query);
    echo ($query);
    $this->disconnect();
  }
  public function isUniqueSKU($SKU) {
    $this->connect();
    $SKU = mysqli_real_escape_string($this->connection,$SKU);
    $query = "Select * FROM products ";
    $query .= "WHERE SKU='" . $SKU . "' ";
    $result = mysqli_query($this->connection,$query);
    $found;
    if($result->num_rows) {
      $found = true;
    } else {
      $found = false;
    }
    $result->free_result();
    $this->disconnect();
    return $found;
  }
}
?>
