<?php
abstract class Product {
  public $SKU;
  public $Name;
  public $Price;

  abstract protected function setAttributes($Attributes);
  abstract public function printAttribute();
  abstract public function printAttributeForm();
  abstract public function prepareAttributes();
  abstract public function numberProperties();
  public function setProperties($SKU, $Name, $Price, $Attributes) {
    $this->SKU =$SKU;
    $this->Name =$Name;
    $this->Price =$Price;
    $this->setAttributes($Attributes);
  }
  public function printProduct() {
    echo '<div class="productItem" data-SKU="'.$this->SKU.'">';
    echo '<div class="itemContent">';
    echo '<h1>'.$this->SKU.'</h1>';
    echo '<h2>'.$this->Name.'</h2>';
    echo '<h3>'.$this->Price.' $</h3>';
    $this->printAttribute();
    echo '</div>';
    echo '</div>';
  }
  protected function printFormField($Type, $Name, $Unit = "") {
    echo '<div class="row">';
    if($Unit) {
      echo '<label for="' . $Name . '">' . $Name. ' (' . $Unit .')'. '</label>';
    } else {
      echo '<label for="' . $Name . '">' . $Name . '</label>';
    }
    echo '<div class="inputWrapper">';
    echo '<input type="' . $Type . '" class="form-control" name="' . $Name . '">';
    echo '</div>';
    echo '</div>';
  }
  private function prepareFormData($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  private function isPositiveNumber($value) {
     if ( is_numeric($value) && (float)$value > 0 ) {
       return true;
     }
     return false;
  }
  public function validateForm() {
    $errors = [];
    $numberInputs = $this->numberProperties();
    foreach ($_POST as $key => $value) { // Check if blank value
      if($key == 'function' || $key == 'Type')
        continue;
      $value = $this->prepareFormData($value);
      $_POST[$key] = $value;
      if( !isset($value) || $value === '' ) {
        $errors[] = $key." is required. Please, submit required data";
      } elseif( $key == 'SKU') {
        $db = new db();
        if($db->isUniqueSKU($value))
          $errors[] = "SKU must be unique.";
      } elseif ( array_key_exists( $key, $numberInputs) ) {
        if( !$this->isPositiveNumber($value) )
          $errors[] = $key." must be a positive number.";
      }
    }
    return $errors;
  }
}

?>
