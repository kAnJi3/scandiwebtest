<?php
class Book extends Product {
  public $Weight;
  protected function setAttributes($Attributes) {
    $this->Weight =$Attributes;
  }
  public function printAttribute() {
    echo '<h3> Weight:'.$this->Weight.'KG</h3>';
  }
  public function printAttributeForm() {
    $this->printFormField('number','Weight', "KG");
    echo "<p>Please provide Weight in KG</p>";
  }
  public function prepareAttributes() {
    return $_POST['Weight'];
  }
  public function numberProperties() {
    return array_fill_keys( array('Price', 'Weight'), NULL );
  }
}
?>
