<?php
class DVD_disc extends Product {
  public $Size;
  protected function setAttributes($Attributes) {
    $this->Size =$Attributes;
  }
  public function printAttribute() {
    echo '<h3> Size:'.$this->Size.' MB</h3>';
  }
  public function printAttributeForm() {
    $this->printFormField('number','Size', "MB");
    echo "<p>Please provide Size in MB</p>";
  }
  public function prepareAttributes() {
    return $_POST['Size'];
  }
  public function numberProperties() {
    return array_fill_keys( array('Price', 'Size'), NULL );
  }
}
?>
