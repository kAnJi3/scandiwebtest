<?php
class Furniture extends Product {
  public $Dimensions;
  protected function setAttributes($Attributes) {
    $this->Dimensions = json_decode($Attributes);
  }
  public function printAttribute() {
    echo '<h3> Dimensions:'. implode('x', $this->Dimensions) .'</h3>';
  }
  public function printAttributeForm() {
    $this->printFormField('number','Height', "CM");
    $this->printFormField('number','Width', "CM");
    $this->printFormField('number','Length', "CM");
    echo "<p>Please provide dimensions HxWxL format</p>";
  }
  public function prepareAttributes() {
    $Attributes = array( intval($_POST['Height']), intval($_POST['Width']), intval($_POST['Length']) );
    return json_encode($Attributes);
  }
  public function numberProperties() {
    return array_fill_keys( array('Price', 'Height', 'Width', 'Length'), NULL );
  }
}
?>
